"use strict";

var KillBirds = function () {};

KillBirds.prototype = {
    init: function () {},
    set: function (obj) {
        var defaultData = JSON.parse(LocalContractStorage.get('infoList'));
        var data = Object.prototype.toString.call(defaultData) == '[object Array]' ? defaultData : [];
        data.push({
            username: obj.username,
            step: obj.step,
            date: obj.date
        });
        if (data.length > 1) {
            LocalContractStorage.del('infoList');
        };
        LocalContractStorage.set('infoList', JSON.stringify(data));
    },
    get: function () {
        return LocalContractStorage.get('infoList');
    },
};

module.exports = KillBirds;